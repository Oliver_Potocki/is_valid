#include "tree_based_quadrants.h"
#include <math.h>

int is_valid(const quadrant_t * q){

return ((q -> x & (int) pow(2, MAXLEVEL- q -> level)) == 0) 
&&  ((q -> y & (int) pow(2, MAXLEVEL- q -> level)) == 0) 
&& ((q -> z & (int) pow(2, MAXLEVEL- q -> level)) == 0);

}
